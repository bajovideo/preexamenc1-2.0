document.addEventListener('DOMContentLoaded', function () {
    const breedsSelect = document.getElementById('breeds');
    const loadBreedButton = document.getElementById('loadBreedButton');
    const showImageButton = document.getElementById('showImageButton');
    const dogImage = document.getElementById('dogImage');

    // Función para cargar las razas en el combobox
    async function loadBreeds() {
        try {
            const response = await fetch('https://dog.ceo/api/breeds/list');
            const data = await response.json();
            const breeds = data.message;

            breeds.forEach(breed => {
                const option = document.createElement('option');
                option.value = breed;
                option.textContent = breed;
                breedsSelect.appendChild(option);
            });
        } catch (error) {
            console.error('Error al cargar las razas:', error);
        }
    }

    // Función para cargar una imagen de la raza seleccionada
    async function loadBreedImage() {
        const selectedBreed = breedsSelect.value;
        try {
            const response = await fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`);
            const data = await response.json();
            const imageUrl = data.message;

            dogImage.src = imageUrl;
        } catch (error) {
            console.error('Error al cargar la imagen:', error);
        }
    }

    // Evento para cargar las razas al cargar la página
    loadBreeds();

    // Evento para habilitar el botón "Mostrar Imagen" al cargar una raza
    loadBreedButton.addEventListener('click', function () {
        loadBreedImage();
        showImageButton.disabled = false;
    });

    // Evento para mostrar una imagen al presionar el botón "Mostrar Imagen"
    showImageButton.addEventListener('click', loadBreedImage);
});
